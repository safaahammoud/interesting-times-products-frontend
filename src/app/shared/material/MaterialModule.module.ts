import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SnackbarComponent } from '../components/snackbar/snackbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';

const MODULES = [
  CommonModule,
  MatTabsModule,
  MatGridListModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatSnackBarModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatInputModule,
  MatToolbarModule,
  FlexLayoutModule
];
@NgModule({
  declarations: [SnackbarComponent],
  imports: [
   ...MODULES
  ],
  exports: [...MODULES ],
  entryComponents: [SnackbarComponent]
})
export class MaterialModule { }
