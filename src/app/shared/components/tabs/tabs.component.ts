import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Product } from 'src/app/models/Product';
import { Router } from '@angular/router';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { BehaviorSubject, of } from 'rxjs';
import { takeUntil, takeWhile, catchError } from 'rxjs/operators';
@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabsComponent implements OnInit, OnChanges {
  public tabTitleArray;
  public products;
  @Output() actionButtonEmitter: EventEmitter<{ id: number, index: number }> = new EventEmitter();
  @Output() selectedIndexChange: EventEmitter<MatTabChangeEvent> = new EventEmitter();
  @Input('tabsTitle') set tabsTitle(tabTitleArray: Array<{ key: string, value: string }>) {
    this.tabTitleArray = tabTitleArray;
  }
  get tabsTitle() {
    return this.tabTitleArray;
  }
  public data = new BehaviorSubject<Product[]>([]);
  @Input() set cardsList(cards: Array<Product>) {
    this.data.next(cards);
  }
  get cardsList() {
    return this.data.getValue();
  }

  constructor(private router: Router, private dialog: MatDialog) { }

  public ngOnInit() {
    // this.data.pipe(
    //   takeWhile(() => !this.products),
    //   catchError(err => {
    //     this.router.navigate(['/error', err.message]);
    //     return of([])
    //   })
    // ).subscribe(x => {
    //   this.products = x;
    // });
  }

  public ngOnChanges(simpleChanges: SimpleChanges) {
  }

  public titleClicked(event) {
    this.selectedIndexChange.emit(event);
  }

  public navigateToEdit(id: number) {
    this.router.navigate(['product-edit/' + id]);
  }

  public deleteProduct(productId: number, product: Product) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '500px',
      data: { message: 'Are you sure you want to delete this record?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.actionButtonEmitter.emit({ id: productId, index: this.cardsList.indexOf(product) });
      }
    });
  }

}
