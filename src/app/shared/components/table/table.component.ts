import { Component, OnInit, Input, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { Product } from 'src/app/models/Product';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  dataSource;
  myData;
  myDataLength;
  @Input('myDataArray') set myDataArray(myData) {
    this.dataSource = new MatTableDataSource (myData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  get myDataArray() {
    return this.myData;
  }
  @Input() columnsToDisplay;
  @Input() columns;
  @Output() pageEmitter: EventEmitter<number> = new EventEmitter();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor() {

   }

  ngOnInit() {

  }
  ngAfterViewInit() {
    this.paginator.page.subscribe(() => {
      this.pageEmitter.emit(this.paginator.pageSize);
    });
  }
  applyFilter(filterValue: string) {
    // this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate =
    (data: Product, filter: string) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
    this.dataSource.filterPredicate({}, filterValue);

  }

}
