import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from './auth-service/authentication.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['ner@gmail.com', Validators.required],
      password: ['P@ssw0rd', Validators.required]
    });

    // logout the person when he opens the app for the first time
    this.authenticationService.logout();
  }

    // convenience getter for easy access to form fields
    get form(){
      return this.loginForm.controls;
    }

   // on submit
   onSubmit(){
    this.authenticationService.login(this.loginForm.value)
    .subscribe(
        data => {
        },
        error => {
            this.error = error;
            this.loading = false;
        }
    );
  }

}
