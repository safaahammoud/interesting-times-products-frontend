import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, tap } from "rxjs/operators";
import { Tokens } from "src/app/models/tokens";
import { FormBuilder, FormGroup } from "@angular/forms";
// authentication service is used to LOGIN and LOGOUT of the application
// it posts the creds (username and password) to the backend and check for the response if it has JWT token
// if the response from the backend has jwt token, then the authentication was succesful
// on successful authentication, the user details are stored in the local storage + jwt token

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  public apiUrl = "http://localhost:8000/api";
  private readonly JWT_TOKEN = "JWT_TOKEN";
  private readonly REFRESH_TOKEN = "REFRESH_TOKEN";

  constructor(private http: HttpClient) {}
  public requestCookie() {
    return this.http.get("sanctum/csrf-cookie");
  }

  public login(form) {
    return this.http.post<any>(`${this.apiUrl}/login`, form);
  }

  logout() {
    // remove user from local storage
    localStorage.removeItem("currentUser");
  }

  refreshToken() {
    return this.http
      .post<any>(`${this.apiUrl}/refresh`, {
        refreshToken: this.getRefreshToken(),
      })
      .pipe(
        tap((tokens: Tokens) => {
          this.storeJwtToken(tokens.jwt);
        })
      );
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }
}
