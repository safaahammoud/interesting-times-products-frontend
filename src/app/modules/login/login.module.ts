import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularModule } from 'src/app/shared/AngularModule.module';
import { MaterialModule } from 'src/app/shared/material/MaterialModule.module';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    AngularModule,
    MaterialModule
  ]
})
export class AuthenticationModule { }
