import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Product } from 'src/app/models/Product';
import { ProductService } from '../products/service/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements OnInit {
  imageURL;
  editMode: boolean;
  productForm: FormGroup;
  productDetails: Product;
  selectedFile: File = null;
  productObservable$: Observable<Product>;

  public constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private productService: ProductService,
  ) { }

  public ngOnInit(): void {
    this.initializeVariables();
    this.subscribeToRouter();
  }

  private initializeVariables(): void {
    this.editMode = false;
    this.productForm = this.fb.group({
      productName: ['', [Validators.required]],
      image: [''],
      old_price: ['', [Validators.maxLength(7), Validators.pattern(/[0-9]/i)]],
      updateOn: 'blur'
    });
    this.imageURL = '';
  }

  private subscribeToRouter() {
    return this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.editMode = params.has('id');
        if (this.editMode) {
          const productId = +params.get('id');
          return this.productService.getProductById(productId);
        }
        return of(new Product());
      })
    ).subscribe((product) => {
      this.productDetails = product;
      this.productForm.controls.productName.setValue(this.productDetails.productName);
      (this.productDetails.discount_percent !== 0) ?
        this.productForm.controls.old_price.setValue(this.productDetails.new_price) :
        this.productForm.controls.old_price.setValue(this.productDetails.old_price);
      this.imageURL = this.productDetails.image;
    });

  }

  public onFormSubmit(): void {
    if (!this.editMode) {
      const product = {
        productName: this.productForm.controls.productName,
        image: this.productForm.controls.image,
        old_price: this.productForm.controls.old_price,
      }

      this.productService.addProduct(product).subscribe((response) => {
        console.log(response);
        return;
        // this.openSnackBar(response);
        setTimeout(() => {
          this.router.navigate(['/product-list']);
        }, 1000);
      });
    } else {
      console.log(456);
      const formData = new FormData();
      formData.append('productName', this.productForm.get('productName').value);
      formData.append('image', this.productForm.get('image').value);
      formData.append('old_price', this.productForm.get('old_price').value);
      (this.productDetails.discount_percent !== 0) ?
        formData.append('new_price', this.productForm.get('old_price').value) :
        formData.append('old_price', this.productForm.get('old_price').value);
      formData.append('id', this.productDetails.id);

      this.productService.updateProduct(formData)
        .subscribe(success => {
          this.openSnackBar(success.response);
          setTimeout(() => {
            this.router.navigate(['/product-list']);
          }, 1000);
        },
          error => this.openSnackBar('Error Occurred'));
    }
  }

  public onFileSelected(event): void {
    this.selectedFile = (event.target as HTMLInputElement).files[0];
    this.productForm.controls.image.setValue(this.selectedFile);
    this.productForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  public openSnackBar(msg: string): void {
    this._snackBar.open(msg, ''
      , {
        duration: 5000
      });
  }

}
