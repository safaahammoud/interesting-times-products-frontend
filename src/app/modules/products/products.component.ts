import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProductService } from './service/product.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Product } from 'src/app/models/Product';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  username: string;
  products: Array<Product>;
  currentYear: string;
  productsImageTitle: string;
  tabsTitle: Array<{ key: string, value: string }>;

  constructor(private productService: ProductService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.tabsTitle = [{ key: 'allproducts', value: 'ALL PRODUCTS' },
    { key: 'discounted', value: 'DISCOUNT' }];
    this.currentYear = '2020';
    this.productsImageTitle = 'Enjoy Summer offer';

    this.getAllProducts();
  }

  private getAllProducts() {
    this.productService.getProducts().subscribe({
      next: (response) => {
        this.products = response.products;
      }
    });
  }

  private getDiscountedProducts() {
    this.productService.getDiscountedProducts().subscribe(response => {
      this.products = response.products;
    });
  }

  public titleClicked(tabIndex) {
    if (tabIndex === 0) {
      this.getAllProducts();
    } else if (tabIndex === 1) {
      this.getDiscountedProducts();
    }
  }

  public ondeleteClick(event) {
    this.products.splice(event.index, 1);
    this.productService.deleteProduct(event.id).subscribe(success => this.openSnackBar(success.response),
      error => this.openSnackBar('Error Occurred'));
  }

  public openSnackBar(msg: string) {
    this._snackBar.open(msg, ''
      , {
        duration: 5000
      });
  }
}
