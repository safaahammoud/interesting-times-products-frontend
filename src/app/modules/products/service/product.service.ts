import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../../../models/Product';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = 'http://localhost:8000/api/products';
  constructor(private http: HttpClient) {
  }

  public getProducts(): Observable<{ products: Product[] }> {
    const httpOptions = {
      headers: new HttpHeaders()
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    return this.http.get<{ products: Product[] }>(this.apiUrl, httpOptions)
      .pipe(
        catchError(this.handleError('getProducts', { products: [] }))
      );
  }

  public getDiscountedProducts() {
    const httpOptions = {
      headers: new HttpHeaders()
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    return this.http.get<{ products: Product[] }>(this.apiUrl + '/discounted', httpOptions)
      .pipe(
        catchError(this.handleError('getProducts', { products: [] }))
      );
  }

  public addProduct(requestParam: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders()
    };
    httpOptions.headers.append('Content-Type', 'application/json');
    httpOptions.headers.append('Access-Control-Allow-Origin', '*');
    return this.http.post<any>(this.apiUrl, requestParam);
  }

  public getProductById(id: number): Observable<Product> {
    return this.http.get<Product>(this.apiUrl + '/' + id);
  }

  public updateProduct(formData: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', '*');
    const url = `${this.apiUrl}/updateModel`;
    return this.http.post(url, formData).pipe(
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  public deleteProduct(id): Observable<{ response: string }> {
    const url = `${this.apiUrl}/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', '*');
    return this.http.delete<{ response: string }>(url, httpOptions).pipe(
      catchError(this.handleError<{ response: string }>('deleteProduct'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  public getCategoriesForProduct() {
    this.apiUrl = 'http://localhost:8000/api/product/1/categories';
    let httpOptions = {
      headers: new HttpHeaders()
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    return this.http.get(this.apiUrl, httpOptions);

  }
  public getTagsRelatedToProductAndCategory() {
    this.apiUrl = 'http://localhost:8000/api/product/category/1/1/tags';
    let httpOptions = {
      headers: new HttpHeaders()
    };
    httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    return this.http.get(this.apiUrl, httpOptions);

  }
}
