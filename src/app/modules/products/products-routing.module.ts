import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products.component';
import { TabsComponent } from 'src/app/shared/components/tabs/tabs.component';

const routes: Routes = [{ path: '', component: ProductsComponent,children:[
  { path: 'tabs', component:TabsComponent },
  { path: 'footer', loadChildren: () => import('../../modules/footer/footer.module').then(m => m.FooterModule), },
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
