import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { TabsComponent } from 'src/app/shared/components/tabs/tabs.component';
import { MaterialModule } from 'src/app/shared/material/MaterialModule.module';
import { FooterModule } from '../footer/footer.module';

@NgModule({
  declarations: [
      ProductsComponent,
      TabsComponent
    ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    MaterialModule,
    FooterModule
  ]
})
export class ProductsModule { }
