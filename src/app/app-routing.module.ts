import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'product-list', pathMatch: 'full' },
  {
    path: 'product-list', loadChildren: () =>
      import('./modules/products/products.module').then(m => m.ProductsModule),

  },
  {
    path: 'add-product', loadChildren: () =>
      import('./modules/add-product/add-product.module').then(m => m.AddProductModule)
  },
  {
    path: 'error', loadChildren: () =>
      import('./modules/add-product/add-product.module').then(m => m.AddProductModule)
  },
  {
    path: 'product-edit/:id', loadChildren: () =>
      import('./modules/add-product/add-product.module').then(m => m.AddProductModule)
  },
  { path: 'login', loadChildren: () => import('./modules/login/login.module').then(m => m.AuthenticationModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
