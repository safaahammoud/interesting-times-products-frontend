import { Category } from './Category';

export class Product {
    id: any;
    productName: string;
    image: File;
    old_price: number;
    new_price: number;
    discount_percent: number;
    category: Category;
}
